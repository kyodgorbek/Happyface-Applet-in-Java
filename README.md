# Happyface-Applet-in-Java



import java.awt.*;
import java.Applet.applet;

public class HappyFace extends Applet {
    // Data fields
    private int maxX = 500;
    private int maxY = 400;
    private int headX = maxX / 2;
    private int headY = maxY / 2;
    private int headRadius = maxY / 4;
    private int leftEyex = headX - headRadius / 4;
    private int rightEyex = headX + headRadius / 4;
    private int eyeY = headY - headRadius / 4;
    private int eyeRadius = headRadius / 10;
    private int noseX = headX;
    private int noseY = headY + headRadius / 4;
    private int noseRadius = eyeRadius;
    protected int smileRadius = (int) Math.round(0.75 * headRadius);
    
    // Methods
    public void paint(Graphics g){
     g.setColor(Color.black);
     
    // Draw head
    g.drawOval(headX - headRadius, headY - headRadius, 2 * eyeRadius, 2 * headRadius);
    
    // Draw left eye
    g.drawOval(leftEyex - eyeRadius, eyeY - eyeRadius , 2 * eyeRadius, 2 * eyeRadius);
    
    // Draw righ eye
    g.drawOval(rightEyeX - eyeRadius, eyeY - eyeRadius, 2 * eyeRadius, 2 * eyeRadius);
    
    // Draw nose
    g.drawOval(noseX - noseRadius, noseY - noseRadius, 2 * noseRadius , 2 * noseRadius);
    
    // Draw smile
    g.drawArc(headX - smileRadius, headY - smileRadius, 2 * smileRadius, 2 * smileRadius, 210, 120);
  }
 }       
    
    
